﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum GridElementFill
{
    Empty = 0,
    Yellow = 1,
    Red = 2
}

public class MainGameManager : Singleton<MainGameManager>
{
    [SerializeField] protected Transform m_GridElementsParent;

    [SerializeField] protected GameObject m_GridElementPrefab;

    [SerializeField] protected GameObject m_GameOverPanel;

    [SerializeField] protected Text m_ResultText;

    [SerializeField] protected Button m_RestartButton;

    [SerializeField] protected Button m_MainMenuButton;

    protected int numRows;
    public int NumRows
    {
        get { return numRows; }
    }

    protected int numColumns;
    public int NumColumns
    {
        get { return numColumns; }
    }

    protected GridElementFill[,] gameBoardGridFillArray;
    public GridElementFill[,] GameBoardGridFillArray
    {
        get { return gameBoardGridFillArray; }
        set { gameBoardGridFillArray = value; }
    }

    protected Vector3[,] gridPositionArray;
    public Vector3[,] GridPositionArray
    {
        get { return gridPositionArray; }
    }

    protected int numPiecesToWin = 4;

    protected string gameResultText;

    // Start is called before the first frame update
    void Start()
    {
        m_RestartButton.onClick.AddListener(RestartGame);
        m_MainMenuButton.onClick.AddListener(GoToMainMenu);

        numRows = GameSettings.Instance.GetNumberOfRows();
        numColumns = GameSettings.Instance.GetNumberOfColumns();
        CreateGameBoard();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
            RestartGame();
    }

    void CreateGameBoard()
    {
        gameBoardGridFillArray = new GridElementFill[numRows, numColumns];
        gridPositionArray = new Vector3[numRows, numColumns];

        float startingX = (float) -(numColumns - 1) / 2;
        float startingY = (float) (numRows - 1) / 2;

        for (int i = 0; i < numRows; i++)
        {
            float x = startingX;
            float y = startingY - i;

            for (int j = 0; j < numColumns; j++)
            {
                gameBoardGridFillArray[i, j] = GridElementFill.Empty;
                gridPositionArray[i, j] = new Vector3(x + j, y, -1);

                GameObject instGrid = Instantiate(m_GridElementPrefab);
                instGrid.transform.SetParent(m_GridElementsParent);
                instGrid.transform.position = gridPositionArray[i, j];
                instGrid.transform.localRotation = Quaternion.identity;
            }
        }

        StartCoroutine(CameraResize.Instance.ResizeCamera(gridPositionArray[0, 0]));

        StartGame();
    }

    void StartGame()
    {
        InteractionManager.Instance.ChangeTurn();
    }

    public void GameOver(bool isPlayerTurn, bool draw = false)
    {
        m_GameOverPanel.SetActive(true);
        if (!draw)
            m_ResultText.text = string.Format("{0} won", isPlayerTurn ? "Player" : "Bot");
        else
            m_ResultText.text = "Draw";
    }

    void RestartGame()
    {
        m_GameOverPanel.SetActive(false);
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    void GoToMainMenu()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

    public bool AreEmptySlotsAvailable()
    {
        for (int i = 0; i < numRows; i++)
        {
            for (int j = 0; j < numColumns; j++)
            {
                if (gameBoardGridFillArray[i, j] == GridElementFill.Empty)
                    return true;
            }
        }

        return false;
    }

    public bool CheckForWinSequence(bool isPlayerTurn)
    {
        int layerMask = isPlayerTurn ? (1 << 8) : (1 << 9);
        float diagonalDistance = Vector2.Distance(Vector2.zero, Vector2.one * 3f);

        for (int i = 0; i < numRows; i++)
        {
            for (int j = 0; j < numColumns; j++)
            {
                if (gameBoardGridFillArray[i, j] == GridElementFill.Empty)
                    continue;

                // Horizontal check
                RaycastHit2D[] raycastHits = Physics2D.RaycastAll(gridPositionArray[i, j], Vector2.right, 3f, layerMask);
                if (raycastHits.Length == 4)
                    return true;

                // Vertical check
                raycastHits = Physics2D.RaycastAll(gridPositionArray[i, j], Vector2.down, 3f, layerMask);
                if (raycastHits.Length == 4)
                    return true;

                //Diagonal check
                raycastHits = Physics2D.RaycastAll(gridPositionArray[i, j], new Vector2(1, -1), diagonalDistance, layerMask);
                if (raycastHits.Length == 4)
                    return true;

                //Diagonal check
                raycastHits = Physics2D.RaycastAll(gridPositionArray[i, j], new Vector2(-1, -1), diagonalDistance, layerMask);
                if (raycastHits.Length == 4)
                    return true;
            }
        }

        return false;
    }

    public int GetTokenDropColumnIndex(float newTokenXPos, bool isPlayerTurn)
    {
        float columnWidth = 1f;
        float columnHalfWidth = columnWidth / 2;
        int columnIndex = -1;

        for (int i = 0; i < MainGameManager.Instance.NumColumns; i++)
        {
            if (newTokenXPos <= gridPositionArray[0, i].x + columnHalfWidth)
            {
                columnIndex = i;
                break;
            }
        }

        if (!isPlayerTurn)
            columnIndex = Utility.GetRandomInt(0, MainGameManager.Instance.NumColumns);

        return columnIndex;
    }

    public int GetFreeGridRowIndex(int columnIndex)
    {
        int freeRowIndex = -1;

        for (int i = MainGameManager.Instance.NumRows - 1; i >= 0; i--)
        {
            if (gameBoardGridFillArray[i, columnIndex] == GridElementFill.Empty)
            {
                freeRowIndex = i;
                break;
            }
        }

        return freeRowIndex;
    }
}