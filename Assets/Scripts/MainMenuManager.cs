﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField] protected Button m_StartGameButton;

    // Start is called before the first frame update
    void Start()
    {
        m_StartGameButton.onClick.AddListener(() =>
        {
            StartGameButtonClicked();
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// This function is executed on start game button is clicked.
    /// </summary>
    void StartGameButtonClicked()
    {
        // Loads the scene asynchronously. Asynchronous operation is needed when the loading scene is big and showing the progress bar.
        SceneManager.LoadSceneAsync(1, LoadSceneMode.Single);
    }
}
