﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SettingsDataStructure
{
    public int NumberOfRows;
    public int NumberOfColumns;
}

public class GameSettings : Singleton<GameSettings>
{
    protected SettingsDataStructure settingsDataStructure = new SettingsDataStructure();
    
    void Awake()
    {
        LoadSettings();
    }

    void LoadSettings()
    {
        TextAsset textAsset = Resources.Load<TextAsset>("GameSettings");
        string textDataFromFile = textAsset.text;

        settingsDataStructure = JsonUtility.FromJson<SettingsDataStructure>(textDataFromFile);
    }

    public int GetNumberOfRows()
    {
        return settingsDataStructure.NumberOfRows;
    }

    public int GetNumberOfColumns()
    {
        return settingsDataStructure.NumberOfColumns;
    }
}
