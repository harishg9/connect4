﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraResize : Singleton<CameraResize>
{
    [SerializeField] protected float m_Smoothness;

    [SerializeField] protected float m_BoundaryOffset;

    protected Camera camera;

    public Camera CurrentCamera
    {
        get { return camera; }
    }

    // Start is called before the first frame update
    void Start()
    {
        camera = this.GetComponent<Camera>();
    }

    public void UpdateOrthographicSize(float size)
    {
        if (camera.orthographic)
            camera.orthographicSize = size;
    }

    public IEnumerator ResizeCamera(GameObject gameObject)
    {
        yield return new WaitForEndOfFrame();

        while (!gameObject.GetComponent<Renderer>().isVisible)
        {
            yield return new WaitForEndOfFrame();
            camera.orthographicSize += 0.1f;
        }
    }

    public IEnumerator ResizeCamera(Vector3 position)
    {
        yield return new WaitForEndOfFrame();

        Vector3 viewPortPosition = camera.WorldToViewportPoint(position + Vector3.left * (0.5f + m_BoundaryOffset));

        while (viewPortPosition.x <= 0f)
        {
            yield return new WaitForEndOfFrame();
            camera.orthographicSize += m_Smoothness * Time.deltaTime;
            viewPortPosition = camera.WorldToViewportPoint(position + Vector3.left * (0.5f + m_BoundaryOffset));
        }
    }
}
