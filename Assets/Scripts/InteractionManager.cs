﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionManager : Singleton<InteractionManager>
{
    [SerializeField] protected GameObject m_YellowTokenPrefab;

    [SerializeField] protected GameObject m_RedTokenPrefab;

    protected bool isPlayerTurn = false;

    protected bool isTokenDropped = false;

    protected GameObject newToken;

    // Start is called before the first frame update
    void Start()
    {

    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && isPlayerTurn)
        {
            if (!isTokenDropped)
                isTokenDropped = true;
        }
    }

    public void ChangeTurn()
    {
        isPlayerTurn = !isPlayerTurn;
        newToken = GetNewToken();
        StartCoroutine(PositionToken());
    }

    IEnumerator PositionToken()
    {
        yield return null;
        
        if (isPlayerTurn)
        {
            while (!isTokenDropped)
            {
                Vector3 mousePosition = Input.mousePosition;
                Vector3 mousePositionInWorld = CameraResize.Instance.CurrentCamera.ScreenToWorldPoint(mousePosition);
                mousePositionInWorld.x = Mathf.Clamp(mousePositionInWorld.x, -(MainGameManager.Instance.NumColumns - 1) / 2, (MainGameManager.Instance.NumColumns - 1) / 2);
                newToken.transform.position = new Vector3(mousePositionInWorld.x, (float)(MainGameManager.Instance.NumRows + 1) / 2, 0f);

                yield return new WaitForEndOfFrame();
            }

            isTokenDropped = false;
        }

        int droppedTokenColumnIndex = MainGameManager.Instance.GetTokenDropColumnIndex(newToken.transform.position.x, isPlayerTurn);
        int freeGridRowIndex = MainGameManager.Instance.GetFreeGridRowIndex(droppedTokenColumnIndex);

        StartCoroutine(DropToken(freeGridRowIndex, droppedTokenColumnIndex));
    }

    IEnumerator DropToken(int rowIndex, int columnIndex)
    {
        if (rowIndex == -1)
        {
            StartCoroutine(PositionToken());
            yield break;
        }

        yield return null;

        Vector3 endPosition = MainGameManager.Instance.GridPositionArray[rowIndex, columnIndex];
        endPosition.z = 0f;
        Vector3 startPosition = new Vector3(endPosition.x, newToken.transform.position.y, newToken.transform.position.z);

        newToken.transform.position = startPosition;

        yield return new WaitForEndOfFrame();

        MainGameManager.Instance.GameBoardGridFillArray[rowIndex, columnIndex] = GridElementFill.Red;

        iTween.MoveTo(newToken, iTween.Hash("position", endPosition, "islocal", false, "time", 1f, "oncomplete", "OnTokenMoveTweenComplete", "oncompletetarget", this.gameObject));
    }

    void OnTokenMoveTweenComplete()
    {
        if (MainGameManager.Instance.CheckForWinSequence(isPlayerTurn))
            MainGameManager.Instance.GameOver(isPlayerTurn);
        else
        {
            if (MainGameManager.Instance.AreEmptySlotsAvailable())
                ChangeTurn();
            else
                MainGameManager.Instance.GameOver(isPlayerTurn, true);
        }
    }

    GameObject GetNewToken()
    {
        GameObject instObj = Instantiate(isPlayerTurn ? m_RedTokenPrefab : m_YellowTokenPrefab);
        instObj.transform.SetParent(this.transform);
        instObj.transform.position = new Vector3(0f, (float)(MainGameManager.Instance.NumRows + 1) / 2, 0f);
        instObj.transform.rotation = Quaternion.identity;
        instObj.transform.localScale = Vector3.one;

        return instObj;
    }
}